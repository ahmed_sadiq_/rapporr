//
//  Constants.h
//  Rapporr
//
//  Created by Ahmed Sadiq on 20/03/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#ifndef Constant_h
#define Constant_h

#pragma mark Screen Sizes
#define IS_IPHONE_6 ([[UIScreen mainScreen] bounds].size.height == 667)
#define IS_IPHONE_5 ([[UIScreen mainScreen] bounds].size.height == 568)
#define IS_IPHONE_4 ([[UIScreen mainScreen] bounds].size.height == 480)
#define IS_IPAD ([[UIScreen mainScreen] bounds].size.height == 1024)
#define IS_IPHONE_6Plus ([[UIScreen mainScreen] bounds].size.height == 736)

#pragma mark Colors
#define statusBarColor [UIColor colorWithRed:0.996 green:0.341 blue:0.129 alpha:1.0]



/*  ------------------- BASE URL ------------------- */

#if DEBUG
#define BASE_URL            @"https://app.rapporrapp.com"   // Stagging
#else
#define BASE_URL            @"https://app.rapporrapp.com"  // Production
#endif

///

#define URI_VALIDATE_NUMBER             @"/validateMobile?number="
#define URI_AUTHENTICATE_NUMBER         @"/auth/phone"
#define URI_VALIDATE_CODE               @"/auth/device"
#define URI_CREATE_RAPPORR              @"/organisation/create"
#define URI_GET_CONVERSATION            @"/conversations/user/"
#define URI_GET_USERS                   @"/organisation/"

#define RAPPORRVERSION                  @"300"
#define COOKIE                          @"GOOGAPPUID=300"
#define RAPPORR_API_KEY                 @"xpeEfxTJ1cIyeE41"

#endif /* Constants_h */
