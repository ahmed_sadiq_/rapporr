//
//  MessageVC.h
//  Rapporr
//
//  Created by Ahmed Sadiq on 06/04/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MessageModel.h"
#import "VCFloatingActionButton.h"
#import <CoreData/CoreData.h>
#import "AppDelegate.h"

@interface MessageVC : UIViewController<floatMenuDelegate,UISearchDisplayDelegate, UISearchBarDelegate> {
    NSArray *currencies;
    NSMutableArray *messages;
    NSMutableArray *users;
    NSArray *searchResults;
    UITableView *searchTblView;
    NSString *timeStamp;
    AppDelegate *delegate;
    NSString *userTimeStamp;
}
@property (strong, nonatomic) NSManagedObjectContext *objectController;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet UITableView *mainTblView;
@property (strong, nonatomic) VCFloatingActionButton *addButton;
@end
