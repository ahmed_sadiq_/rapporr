//
//  NetworkManager.h
//  Rapporr
//
//  Created by Ahmed Sadiq on 20/03/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetworkManager : NSObject
typedef void (^loadSuccess)(id data);
typedef void (^loadFailure)(NSError *error);

+(void)  validateMobileNumber:(NSString *) uri parameters:(NSString *) number success:(loadSuccess) success failure:(loadFailure) failure;
+(void)  getCompaniesForMobileNumber:(NSString *) uri parameters:(NSDictionary *) params success:(loadSuccess) success failure:(loadFailure) failure;
+(void)  generateVerificationCode:(NSString *) uri parameters:(NSDictionary *) params success:(loadSuccess) success failure:(loadFailure) failure;
+(void)  validatePinCode:(NSString *) uri parameters:(NSDictionary *) params success:(loadSuccess) success failure:(loadFailure) failure;
+(void)  createNewRapporr:(NSString *) uri parameters:(NSDictionary *) params success:(loadSuccess) success failure:(loadFailure) failure;
+(void) fetchConversationAPI:(NSString *) uri parameters:(NSDictionary *) params success:(loadSuccess) success failure:(loadFailure) failure;
@end
