//
//  VerifyRapporrVC.m
//  Rapporr
//
//  Created by Ahmed Sadiq on 22/03/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "VerifyRapporrVC.h"
#import "NetworkManager.h"
#import "RapporrManager.h"
#import "VerifiedCompanyModel.h"
#import "CoreDataController.h"

@interface VerifyRapporrVC ()

@end
#define MAX_LENGTH 6
@implementation VerifyRapporrVC
@synthesize cModel;

- (void)viewDidLoad {
    [super viewDidLoad];
    _companyName.text = cModel.companyName;
    // Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(limitTextField) name:UITextFieldTextDidChangeNotification object:nil];

    [self initViews];

}

- (void) initViews {
    _verificationCode.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Your verification code" attributes:@{NSForegroundColorAttributeName: [UIColor darkGrayColor]}];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark -
#pragma mark Text Field Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if ([textField canResignFirstResponder]) {
        [textField resignFirstResponder];
    }
    
    return YES;
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    // add your method here
    
    return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    [self.view removeGestureRecognizer:tap];
}
- (void)textFieldDidBeginEditing:(UITextField *)textField  {
    [self addTouchGestureForKeyboard];
    
}

-(void)limitTextField
{
    
    if(_verificationCode.text.length >= 6) {
        _verificationCode.text = [_verificationCode.text substringToIndex:6];
        _verifyBtn.backgroundColor = [UIColor colorWithRed:0.996 green:0.341 blue:0.129 alpha:1.0];
        _verifyBtn.enabled = true;
    }
    else {
        _verifyBtn.enabled = false;
        _verifyBtn.backgroundColor = [UIColor lightGrayColor];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField.text.length >= MAX_LENGTH && range.length == 0)
    {
        _verifyBtn.backgroundColor = [UIColor colorWithRed:0.996 green:0.341 blue:0.129 alpha:1.0];
        _verifyBtn.enabled = true;
        return NO; // return NO to not change text
    }
    else
    {
        _verifyBtn.enabled = false;
        _verifyBtn.backgroundColor = [UIColor lightGrayColor];
        return YES;
    }
}
- (void)addTouchGestureForKeyboard {
    tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
}
-(void)dismissKeyboard {
    [self.view endEditing:true];
}

- (IBAction)verifyBtnPressed:(id)sender {
    [self.view endEditing:true];
    NSDictionary *paramsToBeSent = [NSDictionary dictionaryWithObjectsAndKeys:cModel.hostID,@"hostId",_verificationCode.text,@"pincode",@"OS_IOS",@"platform", nil];
    
    [NetworkManager validatePinCode:URI_VALIDATE_CODE parameters:paramsToBeSent success:^(id data) {
        
        NSError* error;
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        
        VerifiedCompanyModel *vcModelTemp = [[VerifiedCompanyModel alloc] initWithDictionary:json];
        [[CoreDataController sharedManager] saveVerifiedCompany:vcModelTemp];
        [RapporrManager sharedManager].vcModel = vcModelTemp;
        
        [self performSegueWithIdentifier:@"mainTabBar" sender:self];
        
    }failure:^(NSError *error) {
        _invalidCodeView.hidden=false;
        NSLog(@"Error");
    }];
}

- (IBAction)invalidCodeOkPressed:(id)sender {
    _invalidCodeView.hidden=true;
}
@end
