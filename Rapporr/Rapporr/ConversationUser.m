//
//  ConversationUser.m
//  Rapporr
//
//  Created by Ahmed Sadiq on 12/04/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "ConversationUser.h"

@implementation ConversationUser
/*
 AvatarUrl = "";
 Deleted = 0;
 Disabled = 1;
 Email = "";
 FName = "";
 FullId = "0001-1025";
 IsRegistered = 0;
 LName = "";
 LastSeen = "0001-01-01T00:00:00Z";
 Name = "\Ue83a#";
 Objects = "";
 Org = "<null>";
 Phone = 123;
 Pin = 805194;
 SmsReceived = 1;
 TimeStamp = "2017-04-12T10:53:14.24592Z";
 UType = Std;
 UserId = 1025;*/


- (id)initWithDictionary:(NSDictionary *) responseData
{
    self = [super init];
    if (self) {
        self.avatarUrl = [self validStringForObject:responseData[@"AvatarUrl"]];
        self.deleted = [self validStringForObject:responseData[@"Deleted"]];
        self.disabled = [self validStringForObject:responseData[@"Disabled"]];
        self.email = [self validStringForObject:responseData[@"Email"]];
        self.fName = [self validStringForObject:responseData[@"FName"]];
        self.fullId = [self validStringForObject:responseData[@"FullId"]];
        self.isRegistered = [self validStringForObject:responseData[@"IsRegistered"]];
        self.lName = [self validStringForObject:responseData[@"LName"]];
        self.lastSeen = [self validStringForObject:responseData[@"LastSeen"]];
        self.name = [self validStringForObject:responseData[@"Name"]];
        self.objects = [self validStringForObject:responseData[@"Objects"]];
        self.org = [self validStringForObject:responseData[@"Org"]];
        self.phone = [self validStringForObject:responseData[@"Phone"]];
        self.pin = [self validStringForObject:responseData[@"Pin"]];
        self.smsRecieved = [self validStringForObject:responseData[@"SmsReceived"]];
        self.timeStamp = [self validStringForObject:responseData[@"TimeStamp"]];
        self.uType = [self validStringForObject:responseData[@"UType"]];
        self.userId = [self validStringForObject:responseData[@"UserId"]];
        
    }
    
    return self;
}

- (id)initWithManagedObject:(NSManagedObject *) conversationUserObj {
    self = [super init];
    if (self) {
        
        self.avatarUrl = [conversationUserObj valueForKey:@"avatarUrl"];
        self.deleted = [conversationUserObj valueForKey:@"userDelete"];
        self.disabled = [conversationUserObj valueForKey:@"disabled"];
        self.email = [conversationUserObj valueForKey:@"email"];
        self.fName = [conversationUserObj valueForKey:@"fName"];
        self.fullId = [conversationUserObj valueForKey:@"fullId"];
        self.isRegistered = [conversationUserObj valueForKey:@"isRegistered"];
        self.lName = [conversationUserObj valueForKey:@"lName"];
        self.lastSeen = [conversationUserObj valueForKey:@"lastSeen"];
        self.name = [conversationUserObj valueForKey:@"name"];
        self.objects = [conversationUserObj valueForKey:@"objects"];
        self.org = [conversationUserObj valueForKey:@"org"];
        self.phone = [conversationUserObj valueForKey:@"phone"];
        self.pin = [conversationUserObj valueForKey:@"pin"];
        self.smsRecieved = [conversationUserObj valueForKey:@"smsRecieved"];
        self.timeStamp = [conversationUserObj valueForKey:@"timeStamp"];
        self.uType = [conversationUserObj valueForKey:@"uType"];
        self.userId = [conversationUserObj valueForKey:@"userId"];
        
    }
    return self;
}

@end
