//
//  ConversationUser.h
//  Rapporr
//
//  Created by Ahmed Sadiq on 12/04/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "BaseEntity.h"

@interface ConversationUser : BaseEntity

@property (strong, nonatomic) NSString *avatarUrl;
@property (strong, nonatomic) NSString *deleted;
@property (strong, nonatomic) NSString *disabled;
@property (strong, nonatomic) NSString *email;
@property (strong, nonatomic) NSString *fName;
@property (strong, nonatomic) NSString *isRegistered;
@property (strong, nonatomic) NSString *lName;
@property (strong, nonatomic) NSString *fullId;
@property (strong, nonatomic) NSString *lastSeen;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *objects;
@property (strong, nonatomic) NSString *org;
@property (strong, nonatomic) NSString *phone;
@property (strong, nonatomic) NSString *pin;
@property (strong, nonatomic) NSString *smsRecieved;
@property (strong, nonatomic) NSString *timeStamp;
@property (strong, nonatomic) NSString *uType;
@property (strong, nonatomic) NSString *userId;

- (id)initWithManagedObject:(NSManagedObject *) conversationUserObj;
- (id)initWithDictionary:(NSDictionary *) responseData;
@end
