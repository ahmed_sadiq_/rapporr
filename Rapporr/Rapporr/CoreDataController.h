//
//  CoreDataController.h
//  Rapporr
//
//  Created by Ahmed Sadiq on 12/04/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "MessageModel.h"
#import "VerifiedCompanyModel.h"
#import "ConversationUser.h"

@interface CoreDataController : NSObject {
    
}
+ (CoreDataController *) sharedManager;

#pragma mark - Save Models
- (void) saveMessageModel : (MessageModel*) mModel;
- (void) saveVerifiedCompany : (VerifiedCompanyModel*) vcModel;
- (void) saveConversationUser : (ConversationUser*) conversationUser;

#pragma mark - Get Models
- (NSMutableArray*) getVerifiedCompanies;

#pragma mark - Utility Methods
- (BOOL) ifAnyVerifiedCompant;


@end
