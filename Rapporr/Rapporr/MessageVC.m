//
//  MessageVC.m
//  Rapporr
//
//  Created by Ahmed Sadiq on 06/04/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "MessageVC.h"
#import "MessageCell.h"
#import "NetworkManager.h"
#import "DataManager.h"
#import "Constants.h"
#import "RapporrManager.h"
#import "VCFloatingActionButton.h"
#import "CoreDataController.h"
#import "ConversationUser.h"
#import "UIImageView+Letters.h"
#import <QuartzCore/QuartzCore.h>

@interface MessageVC ()

@end

@implementation MessageVC
@synthesize addButton;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self adjustTabBarImageOffset];
    [self addFloatingButton];
    
    delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
   
    [self fetchMessagesFromDB];
    [self fetchUserFromDB];
    [_mainTblView reloadData];
    [self getUserList];
}

- (void) fetchUserFromDB {
    NSManagedObjectContext *managedObjectContext = [delegate managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"ConverstionUser"];
    NSMutableArray *tempUsers = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    
    users = [[NSMutableArray alloc] init];
    
    for(int i=0; i<tempUsers.count; i++) {
        NSManagedObject *conversationUserObj = [tempUsers objectAtIndex:i];
        ConversationUser *cUserModel = [[ConversationUser alloc] initWithManagedObject:conversationUserObj];
        [users addObject:cUserModel];
    }
    
    if(users.count >0) {
        ConversationUser *conversationUserObj = [users objectAtIndex:users.count-1];
        
        userTimeStamp = conversationUserObj.timeStamp;
    }
    else {
        userTimeStamp = @"all";
    }
}
- (void) fetchMessagesFromDB {
    
    NSManagedObjectContext *managedObjectContext = [delegate managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Message"];
    NSMutableArray *tempMessages = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    
    messages = [[NSMutableArray alloc] init];
    
    for(int i=0; i<tempMessages.count; i++) {
        NSManagedObject *messageObj = [tempMessages objectAtIndex:i];
        MessageModel *mModel = [[MessageModel alloc] initWithManagedObject:messageObj];
        [messages addObject:mModel];
    }
    
    if(messages.count >0) {
        MessageModel *messageObj = [messages objectAtIndex:messages.count-1];
        timeStamp = messageObj.timeStamp;
    }
    else {
        timeStamp = @"all";
    }
}

- (void) getUserList {
    NSDictionary *paramsToBeSent = [NSDictionary dictionaryWithObjectsAndKeys:[RapporrManager sharedManager].vcModel.hostID,@"hostId",[RapporrManager sharedManager].vcModel.userId,@"userID",nil];
    
    NSString *uri = [NSString stringWithFormat:@"%@%@/users?last=%@",URI_GET_USERS,[RapporrManager sharedManager].vcModel.orgId,userTimeStamp];
    [NetworkManager fetchConversationAPI:uri parameters:paramsToBeSent success:^(id data) {
        
        NSError* error;
        NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData: data options: NSJSONReadingMutableContainers error: &error];
        
        for(int i=0; i<jsonArray.count; i++) {
            ConversationUser *conversationUserModel = [[ConversationUser alloc] initWithDictionary:(NSDictionary*)[jsonArray objectAtIndex:i]];
            [users addObject:conversationUserModel];
            
            [[CoreDataController sharedManager] saveConversationUser:conversationUserModel];
        }
        
        [self getConversationList];
        
    }failure:^(NSError *error) {
        NSLog(@"Error");
    }];
}

- (void) getConversationList {
    NSDictionary *paramsToBeSent = [NSDictionary dictionaryWithObjectsAndKeys:[RapporrManager sharedManager].vcModel.hostID,@"hostId",[RapporrManager sharedManager].vcModel.userId,@"userID",nil];
    
    NSString *uri = [NSString stringWithFormat:@"%@%@?last=%@",URI_GET_CONVERSATION,[RapporrManager sharedManager].vcModel.userId,timeStamp];
    [NetworkManager fetchConversationAPI:uri parameters:paramsToBeSent success:^(id data) {
        
        NSError* error;
        NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData: data options: NSJSONReadingMutableContainers error: &error];
        
        for(int i=0; i<jsonArray.count; i++) {
            MessageModel *mModel = [[MessageModel alloc] initWithDictionary:(NSDictionary*)[jsonArray objectAtIndex:i]];
            [messages addObject:mModel];
            
            [self saveMessageModel:mModel];
        }
        
        [_mainTblView reloadData];
        
    }failure:^(NSError *error) {
        NSLog(@"Error");
    }];
}

- (void) saveMessageModel : (MessageModel*) mModel {
    
    [[CoreDataController sharedManager] saveMessageModel:mModel];
}

- (void) adjustTabBarImageOffset {
    UITabBar *tabBar = self.navigationController.tabBarController.tabBar;
    UITabBarItem *tabBarItem1 = [tabBar.items objectAtIndex:0];
    
    tabBarItem1.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
    self.title = nil;
}

- (void)addFloatingButton {
    CGRect floatFrame = CGRectMake([UIScreen mainScreen].bounds.size.width - 44 - 20, [UIScreen mainScreen].bounds.size.height - 44 - 60, 44, 44);
    
    addButton = [[VCFloatingActionButton alloc]initWithFrame:floatFrame normalImage:[UIImage imageNamed:@"edit"] andPressedImage:[UIImage imageNamed:@"edit"] withScrollview:_mainTblView];
    
    addButton.imageArray = @[@"fb-icon",@"twitter-icon",@"google-icon",@"linkedin-icon"];
    addButton.labelArray = @[@"Facebook",@"Twitter",@"Google Plus",@"Linked in"];
    
    addButton.hideWhileScrolling = YES;
    addButton.delegate = self;
    
    [self.view addSubview:addButton];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark -
#pragma mark Table View Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"Yesterday";
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        searchTblView = self.searchDisplayController.searchResultsTableView;
        return [searchResults count];
        
    } else {
        return [messages count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    MessageCell * cell = (MessageCell *)[tableView dequeueReusableCellWithIdentifier:@"MessageCell"];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"MessageCell" owner:self options:nil];
        cell=[nib objectAtIndex:0];
    }
    
    cell.imgView.layer.cornerRadius = 5;
    cell.imgView.clipsToBounds = YES;
    MessageModel*mModel;
    
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        tableView.frame = _mainTblView.frame;
         mModel = [searchResults objectAtIndex:indexPath.row];
        
    } else {
        mModel = (MessageModel*)[messages objectAtIndex:indexPath.row];
    }
    
    NSMutableArray *usersInMessage = [self usersForMessage:mModel];
    
    
    cell.titleLbl.text = [self getTitleLbl:usersInMessage];
    
    ConversationUser *cUser = [usersInMessage objectAtIndex:usersInMessage.count-1];
    if(cUser.avatarUrl.length > 1) {
        cell.imgView.imageURL = [NSURL URLWithString: cUser.avatarUrl];
        NSURL *url = [NSURL URLWithString:cUser.avatarUrl];
        [[AsyncImageLoader sharedLoader] loadImageWithURL:url];
    }
    else {
        [cell.imgView setImageWithString:cUser.name color:statusBarColor circular:YES];
    }
    
    
    [cell setRightUtilityButtons:[self leftButtons] WithButtonWidth:70.0f];
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
    
}

- (NSString*) getTitleLbl : (NSMutableArray*)usersInMessage {
    NSString *titlelbl = @"";
    if(usersInMessage.count>0){
        ConversationUser *cUser = [usersInMessage objectAtIndex:0];
        titlelbl = cUser.name;
    }
    
    for(int i=1; i<usersInMessage.count; i++){
        ConversationUser *cUser = [usersInMessage objectAtIndex:i];
        titlelbl = [NSString stringWithFormat:@"%@ & %@",titlelbl,cUser.name];
    }
    return titlelbl;
}

- (NSMutableArray *)usersForMessage :(MessageModel*)mModel {
    NSMutableArray *usersArray = [[NSMutableArray alloc] init];
    
    for(int i=0; i<mModel.users.count; i++){
        NSString *userId = [mModel.users objectAtIndex:i];
        ConversationUser *uUser = [self getUserfromUsersList:userId];
        if(uUser)
            [usersArray addObject:uUser];
    }
    
    return usersArray;
}

- (ConversationUser*) getUserfromUsersList :(NSString*)userID {
    for (int i=0; i<users.count; i++) {
        ConversationUser *cUser = [users objectAtIndex:i];
        if([cUser.fullId isEqualToString:userID]){
            return cUser;
        }
    }
    return nil;
}

- (NSArray *)leftButtons
{
    NSMutableArray *leftUtilityButtons = [NSMutableArray new];
    
    [leftUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor colorWithRed:0.113 green:0.443f blue:0.639f alpha:1.0]
                                                icon:[UIImage imageNamed:@"pin"]];
    [leftUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor colorWithRed:0.113f green:0.443f blue:0.639f alpha:1.0]
                                                icon:[UIImage imageNamed:@"archive"]];
    
    return leftUtilityButtons;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 84;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}



- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"name contains[c] %@", searchText];
    searchResults = [messages filteredArrayUsingPredicate:resultPredicate];
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString
                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                      objectAtIndex:[self.searchDisplayController.searchBar
                                                     selectedScopeButtonIndex]]];
    
    return YES;
}

-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    
    self.mainTblView.frame = CGRectMake(0,80,self.mainTblView.frame.size.width, self.mainTblView.frame.size.height);
    return YES;
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar {
    
    self.mainTblView.frame = CGRectMake(0,80,self.mainTblView.frame.size.width, self.mainTblView.frame.size.height);
    return YES;
}

-(void) didSelectMenuOptionAtIndex:(NSInteger)row
{
    NSLog(@"Floating action tapped index %tu",row);
}

#pragma mark - SWTableViewDelegate

- (void)swipeableTableViewCell:(SWTableViewCell *)cell scrollingToState:(SWCellState)state
{
    switch (state) {
        case 0:
            NSLog(@"utility buttons closed");
            break;
        case 1:
            NSLog(@"left utility buttons open");
            break;
        case 2:
            NSLog(@"right utility buttons open");
            break;
        default:
            break;
    }
}

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerLeftUtilityButtonWithIndex:(NSInteger)index
{
    switch (index) {
        case 0:
            NSLog(@"left button 0 was pressed");
            break;
        case 1:
            NSLog(@"left button 1 was pressed");
            break;
        case 2:
            NSLog(@"left button 2 was pressed");
            break;
        case 3:
            NSLog(@"left btton 3 was pressed");
        default:
            break;
    }
}

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index
{
    switch (index) {
        case 0:
        {
            NSLog(@"More button was pressed");
            UIAlertView *alertTest = [[UIAlertView alloc] initWithTitle:@"Hello" message:@"More more more" delegate:nil cancelButtonTitle:@"cancel" otherButtonTitles: nil];
            [alertTest show];
            
            [cell hideUtilityButtonsAnimated:YES];
            break;
        }
        case 1:
        {
            // Delete button was pressed
            //NSIndexPath *cellIndexPath = [self.tableView indexPathForCell:cell];
            
            //[_testArray[cellIndexPath.section] removeObjectAtIndex:cellIndexPath.row];
            //[self.tableView deleteRowsAtIndexPaths:@[cellIndexPath] withRowAnimation:UITableViewRowAnimationLeft];
            break;
        }
        default:
            break;
    }
}

- (BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(SWTableViewCell *)cell
{
    // allow just one cell's utility button to be open at once
    return YES;
}

- (BOOL)swipeableTableViewCell:(SWTableViewCell *)cell canSwipeToState:(SWCellState)state
{
    switch (state) {
        case 1:
            // set to NO to disable all left utility buttons appearing
            return YES;
            break;
        case 2:
            // set to NO to disable all right utility buttons appearing
            return YES;
            break;
        default:
            break;
    }
    
    return YES;
}
@end
