//
//  CoreDataController.m
//  Rapporr
//
//  Created by Ahmed Sadiq on 12/04/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "CoreDataController.h"

@implementation CoreDataController
static CoreDataController *sharedManager;

+ (CoreDataController *) sharedManager
{
    if(sharedManager == nil)
    {
        sharedManager = [[CoreDataController alloc] init];
    }
    
    return sharedManager;
}

- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

- (void) saveMessageModel : (MessageModel*) mModel {
    
    NSManagedObjectContext *context = [self managedObjectContext];
    
    // Create a new managed object
    NSManagedObject *newMessage = [NSEntityDescription insertNewObjectForEntityForName:@"Message" inManagedObjectContext:context];
    [newMessage setValue:mModel.host forKey:@"host"];
    [newMessage setValue:mModel.tags forKey:@"tags"];
    [newMessage setValue:mModel.about forKey:@"about"];
    [newMessage setValue:mModel.create forKey:@"create"];
    [newMessage setValue:mModel.lastMsg forKey:@"lastMsg"];
    [newMessage setValue:mModel.timeStamp forKey:@"timeStamp"];
    [newMessage setValue:mModel.lastMsgId forKey:@"lastMsgId"];
    [newMessage setValue:mModel.cTemplate forKey:@"cTemplate"];
    [newMessage setValue:mModel.senderName forKey:@"senderName"];
    [newMessage setValue:mModel.callBackId forKey:@"callBackId"];
    [newMessage setValue:mModel.startingUser forKey:@"startingUser"];
    [newMessage setValue:mModel.conversationId forKey:@"conversationId"];
    [newMessage setValue:mModel.lastMsgRecieved forKey:@"lastMsgRecieved"];
    [newMessage setValue:mModel.users forKey:@"users"];
    [newMessage setValue:mModel.objects forKey:@"objects"];
    
    
    NSError *error = nil;
    // Save the object to persistent store
    if (![context save:&error]) {
        NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
    }
    
    [[self managedObjectContext] save:nil];
    
}

- (void) saveConversationUser : (ConversationUser*) conversationUser {
    
    NSManagedObjectContext *context = [self managedObjectContext];
    
    // Create a new managed object
    NSManagedObject *newConversationUser = [NSEntityDescription insertNewObjectForEntityForName:@"ConverstionUser" inManagedObjectContext:context];
    [newConversationUser setValue:conversationUser.avatarUrl forKey:@"avatarUrl"];
    [newConversationUser setValue:conversationUser.deleted forKey:@"userDelete"];
    [newConversationUser setValue:conversationUser.disabled forKey:@"disabled"];
    [newConversationUser setValue:conversationUser.email forKey:@"email"];
    [newConversationUser setValue:conversationUser.fName forKey:@"fName"];
    [newConversationUser setValue:conversationUser.fullId forKey:@"fullId"];
    [newConversationUser setValue:conversationUser.isRegistered forKey:@"isRegistered"];
    [newConversationUser setValue:conversationUser.lName forKey:@"lName"];
    [newConversationUser setValue:conversationUser.lastSeen forKey:@"lastSeen"];
    [newConversationUser setValue:conversationUser.name forKey:@"name"];
    [newConversationUser setValue:conversationUser.objects forKey:@"objects"];
    [newConversationUser setValue:conversationUser.org forKey:@"org"];
    [newConversationUser setValue:conversationUser.phone forKey:@"phone"];
    [newConversationUser setValue:conversationUser.pin forKey:@"pin"];
    [newConversationUser setValue:conversationUser.smsRecieved  forKey:@"smsRecieved"];
    [newConversationUser setValue:conversationUser.timeStamp forKey:@"timeStamp"];
    [newConversationUser setValue:conversationUser.uType forKey:@"uType"];
    [newConversationUser setValue:conversationUser.userId forKey:@"userId"];
    
    NSError *error = nil;
    // Save the object to persistent store
    if (![context save:&error]) {
        NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
    }
    
    [[self managedObjectContext] save:nil];
    
}

- (void) saveVerifiedCompany : (VerifiedCompanyModel*) vcModel {
    NSManagedObjectContext *context = [self managedObjectContext];
    
    // Create a new managed object
    NSManagedObject *newCompany = [NSEntityDescription insertNewObjectForEntityForName:@"VerifiedCompany" inManagedObjectContext:context];
    [newCompany setValue:vcModel.companyName forKey:@"companyName"];
    [newCompany setValue:vcModel.expires forKey:@"expires"];
    [newCompany setValue:vcModel.hostID forKey:@"hostID"];
    [newCompany setValue:vcModel.orgId forKey:@"orgId"];
    [newCompany setValue:vcModel.token forKey:@"token"];
    [newCompany setValue:vcModel.userId forKey:@"userId"];
    [newCompany setValue:vcModel.userName forKey:@"userName"];
    [newCompany setValue:vcModel.uType forKey:@"uType"];
    
    NSError *error = nil;
    // Save the object to persistent store
    if (![context save:&error]) {
        NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
    }
    
    [[self managedObjectContext] save:nil];
}

- (BOOL) ifAnyVerifiedCompant {
    
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"VerifiedCompany"];
    NSMutableArray *tempMessages = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    if(tempMessages.count > 0) {
        return true;
    }
    return false;
}

- (NSMutableArray*) getVerifiedCompanies {
    
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"VerifiedCompany"];
    return [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    
}

@end
