//
//  Utils.h
//  Rapporr
//
//  Created by Ahmed Sadiq on 18/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Utils : NSObject

+ (BOOL)validateEmail:(NSString *)candidate;
+ (NSData*)encodeDictionary:(NSDictionary*)dictionary;
+ (NSString*)getTelephonicCountryCode:(NSString *)countryCode;
+ (UIAlertController *) showCustomAlert:(NSString *) title andMessage : (NSString *) message;

@end
