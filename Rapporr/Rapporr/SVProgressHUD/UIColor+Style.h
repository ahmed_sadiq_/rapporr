//
//  UIColor+Style.h
//  Wits
//
//  Created by TxLabz on 08/08/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Style)
+(UIColor *)orangeCutomColor;
+(UIColor *)customGreenButtonColor;
+ (UIColor *) appCutomColor;
@end
