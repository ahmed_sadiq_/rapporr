//
//  MessageModel.m
//  Rapporr
//
//  Created by Ahmed Sadiq on 07/04/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "MessageModel.h"

@implementation MessageModel

@synthesize about,cTemplate,callBackId,conversationId,create,host,lastMsg,lastMsgId,lastMsgRecieved,senderName,startingUser,tags,timeStamp,users,objects;


- (id)initWithDictionary:(NSDictionary *) responseData
{
    self = [super init];
    if (self) {
        self.about = [self validStringForObject:responseData[@"About"]];
        self.cTemplate =[self validStringForObject:responseData[@"CTemplate"]];
        self.callBackId = [self validStringForObject:responseData[@"CallBackid"]];
        self.conversationId =[self validStringForObject:responseData[@"ConversationId"]];
        self.create = [self validStringForObject:responseData[@"Create"]];
        self.host =[self validStringForObject:responseData[@"Host"]];
        self.lastMsg = [self validStringForObject:responseData[@"LastMessage"]];
        self.lastMsgId =[self validStringForObject:responseData[@"LastMessageId"]];
        self.lastMsgRecieved = [self validStringForObject:responseData[@"LastMessageReceived"]];
        self.senderName =[self validStringForObject:responseData[@"SenderName"]];
        self.startingUser = [self validStringForObject:responseData[@"StartingUser"]];
        self.tags =[self validStringForObject:responseData[@"Tags"]];
        self.timeStamp =[self validStringForObject:responseData[@"TimeStamp"]];
        self.users = responseData[@"Users"];
        self.objects = responseData[@"Objects"];
    }
    
    return self;
}
- (id)initWithManagedObject:(NSManagedObject *) messageObj {
    self = [super init];
    if (self) {
        
        self.about = [messageObj valueForKey:@"about"];
        self.cTemplate =[messageObj valueForKey:@"cTemplate"];
        self.callBackId = [messageObj valueForKey:@"callBackId"];
        self.conversationId =[messageObj valueForKey:@"conversationId"];
        self.create = [messageObj valueForKey:@"create"];
        self.host = [messageObj valueForKey:@"host"];
        self.lastMsg = [messageObj valueForKey:@"lastMsgRecieved"];
        self.lastMsgId = [messageObj valueForKey:@"lastMsgId"];
        self.lastMsgRecieved = [messageObj valueForKey:@"lastMsgRecieved"];
        self.senderName = [messageObj valueForKey:@"senderName"];
        self.startingUser = [messageObj valueForKey:@"startingUser"];
        self.tags = [messageObj valueForKey:@"tags"];
        self.timeStamp = [messageObj valueForKey:@"timeStamp"];
        self.users = [messageObj valueForKey:@"users"];
        self.objects = [messageObj valueForKey:@"objects"];
    }
    return self;
}
@end
