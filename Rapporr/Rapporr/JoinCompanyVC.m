//
//  JoinCompanyVC.m
//  Rapporr
//
//  Created by Ahmed Sadiq on 18/03/2017.
//  Copyright © 2017 TxLabz. All rights reserved.
//

#import "VerifyRapporrVC.h"
#import "NetworkManager.h"
#import "JoinCompanyVC.h"
#import "CompanyCell.h"
#import "NewCompanyCell.h"
#import "Constants.h"

@interface JoinCompanyVC ()

@end

@implementation JoinCompanyVC
@synthesize userPhoneNumber;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self fetchCompaniesAssociatedWithNumber];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark -
#pragma mark Table View Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _companyArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NewCompanyCell * cell = (NewCompanyCell *)[_companiesTblView dequeueReusableCellWithIdentifier:@"NewCompanyCell"];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"NewCompanyCell" owner:self options:nil];
        cell=[nib objectAtIndex:0];
    }
    
    CompanyModel *cModel = [_companyArray objectAtIndex: indexPath.row];
    cell.titleLbl.text = cModel.companyName;
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 56;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CompanyModel *cModel = [_companyArray objectAtIndex: indexPath.row];
    [self genetrateVerificationCodeForRapporr:cModel];
}

#pragma mark -
#pragma mark Server Calls

- (void) fetchCompaniesAssociatedWithNumber {
    
    NSDictionary *paramsToBeSent = [NSDictionary dictionaryWithObjectsAndKeys:userPhoneNumber,@"phone",@"OS_IOS",@"platform", nil];
    
    [NetworkManager getCompaniesForMobileNumber:URI_AUTHENTICATE_NUMBER parameters:paramsToBeSent success:^(id data) {
        NSError* error;
        NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData: data options: NSJSONReadingMutableContainers error: &error];
        if(!jsonArray) {
            _tableViewContainer.hidden = true;
        }
        else if (jsonArray.count == 0) {
            _tableViewContainer.hidden = true;
        }
        else {
            _tableViewContainer.hidden = false;
            self.companyArray = [[NSMutableArray alloc] init];
            for(int i=0; i<jsonArray.count; i++) {
                CompanyModel *cModel = [[CompanyModel alloc] initWithDictionary:[jsonArray objectAtIndex:i]];
                [self.companyArray addObject:cModel];
            }
            
            NSSortDescriptor *sortDescriptor;
            sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"companyName" ascending:YES];
            NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
            NSArray *sortedArray;
            sortedArray = [self.companyArray sortedArrayUsingDescriptors:sortDescriptors];
            
            self.companyArray = [[NSMutableArray alloc] initWithArray:sortedArray];
            
            
            [_companiesTblView reloadData];
        }
        
    }failure:^(NSError *error) {
        _tableViewContainer.hidden = true;
        NSLog(@"Error");
    }];
}

- (void) genetrateVerificationCodeForRapporr : (CompanyModel*) cModel {
    selectedCompanyModel = cModel;
    NSDictionary *paramsToBeSent = [NSDictionary dictionaryWithObjectsAndKeys:userPhoneNumber,@"phone",@"OS_IOS",@"platform",cModel.hostID,@"hostId", nil];
    
    [NetworkManager generateVerificationCode:URI_AUTHENTICATE_NUMBER parameters:paramsToBeSent success:^(id data) {
        
        NSError* error;
        
        [self performSegueWithIdentifier:@"verifyRapporr" sender:self];
        
    }failure:^(NSError *error) {
        NSLog(@"Error");
    }];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"verifyRapporr"]) {
        
        VerifyRapporrVC *destSegue = [segue destinationViewController];
        destSegue.cModel = selectedCompanyModel;
    }
}



@end
